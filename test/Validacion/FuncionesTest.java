/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validacion;

import Entidad.Placa;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;


/**
 *
 * @author kenet
 */
public class FuncionesTest {
    
    public FuncionesTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of Validar method, of class Funciones.
     */
    @Test
    public void testValidar() {
        System.out.println("Validar");
        Placa placa = new Placa("AAC-0123");
        Funciones instance = new Funciones();
        instance.Validar(placa);
        // TODO review the generated test code and remove the default call to fail.
        if(!(placa.getLugar_Matriculacion().equals("Azuay"))){
            fail("The test case is a prototype.");
        }else{
            assertEquals(placa.getLugar_Matriculacion() , "Azuay");
        }  
    }
    
}
