/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validarplaca;

import Entidad.Placa;
import Validacion.Funciones;

/**
 *
 * @author kenet
 */
public class Aplicativo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Funciones f = new Funciones();
        
        Placa placa = new Placa("AAC-0123");
        Placa placa2 = new Placa("AEC-0123");
        Placa placa3 = new Placa("AXC-0123");
        Placa placa4 = new Placa("AMC-0123");
        Placa placa5 = new Placa("ABC-0123");
        Placa placa6 = new Placa("CD-0123");
        Placa placa7 = new Placa("IT-0123");
        
        Placa placa8 = new Placa("DBC-0123");
        Placa placa9 = new Placa("KP-0123");
        
        f.Validar(placa);
        f.Validar(placa2);
        f.Validar(placa3);
        f.Validar(placa4);
        f.Validar(placa5);
        f.Validar(placa6);
        f.Validar(placa7);
        f.Validar(placa8);
        f.Validar(placa9);
    }
    
}
