/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author kenet
 */
public class Placa {
    String placa;
    String PL_placa;
    String SL_placa;
    String Lugar_Matriculacion;
    String Tipo_Placa;
    String Color;
    
    public Placa(String placa) {
        this.placa = placa;
        this.PL_placa = placa.charAt(0)+"";
        this.SL_placa = placa.charAt(1)+"";
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPL_placa() {
        return PL_placa;
    }

    public void setPL_placa(String PL_placa) {
        this.PL_placa = PL_placa;
    }

    public String getSL_placa() {
        return SL_placa;
    }

    public void setSL_placa(String SL_placa) {
        this.SL_placa = SL_placa;
    }

    public String getLugar_Matriculacion() {
        return Lugar_Matriculacion;
    }

    public void setLugar_Matriculacion(String Lugar_Matriculacion) {
        this.Lugar_Matriculacion = Lugar_Matriculacion;
    }

    public String getTipo_Placa() {
        return Tipo_Placa;
    }

    public void setTipo_Placa(String Tipo_Placa) {
        this.Tipo_Placa = Tipo_Placa;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    @Override
    public String toString() {
        if(placa.length() == 8)
            return "Placa{" + "placa=" + placa + ", Lugar_Matriculacion=" + Lugar_Matriculacion + ", Tipo_Placa=" + Tipo_Placa + ", Color=" + Color + '}';
        else
            return "Placa{" + "placa=" + placa + ", Tipo_Placa=" + Tipo_Placa + ", Color=" + Color + '}';
    }
    
    
    
}
