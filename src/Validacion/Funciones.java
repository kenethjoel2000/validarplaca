/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validacion;

import Entidad.Placa;
import java.util.HashMap;

/**
 *
 * @author kenet
 */
public class Funciones {
    HashMap<String, String> Ciudad = new HashMap<String, String>();
    HashMap<String, String> Tipo = new HashMap<String, String>();

    public Funciones() {
        LLENAR_Ciudad();
        LLENAR_Tipo();
    }
    public void Validar(Placa placa){
        if(placa.getPlaca().length() == 7 || placa.getPlaca().length() == 8 ){
            if(placa.getPlaca().length() == 7){
                if(this.PLACA_7(placa)){
                    System.out.println("Placa Verificada Correctamente \n" + placa + "\n");  
                }else{
                    System.out.println("" + placa);   
                }
            }else{
                if(this.ValidarPL_P(placa)){
                    if (this.PLACA_8(placa)){
                        placa.toString();
                        System.out.println("Placa Verificada Correctamente \n" + placa + "\n");
                    }else{
                        System.out.println("" + placa + "\n"); 
                    }
                }else{
                    System.out.println("" + placa + "\n"); 
                }
            }
        }else{
            System.out.println("Placa No contiene los digitos correctos");
        }
    }
    private boolean PLACA_8(Placa placa){
        String x = Tipo.get(placa.getSL_placa());
        if (x != null) {
            switch (x) {
                case "Vehiculos Comerciales":
                    placa.setTipo_Placa(x);
                    placa.setColor("Naranja");
                    break;
                case "Vehiculos Gubernamentales":
                    placa.setTipo_Placa(x);
                    placa.setColor("Oro");
                    break;
                case "Vehiculos Gobiernos Autonomos D":
                    placa.setTipo_Placa(x);
                    placa.setColor("Verde-Limon");
                    break;
                case "Vehiculos Uso Oficial":
                    placa.setTipo_Placa(x);
                    placa.setColor("Oro");
                    break;
            }
        } else {
            placa.setTipo_Placa("Vehiculos Comerciales");
            placa.setColor("Blanco-Plateado");
        }
        return true;
    }
    private boolean PLACA_7(Placa placa){
        String y = Tipo.get(placa.getPL_placa() + placa.getSL_placa());
        if (y != null) {
            switch (y) {
                case "Servicio Diplomatico":
                    placa.setTipo_Placa(y);
                    placa.setColor("Azul");
                    break;
                case "Internacion Temporal":
                    placa.setTipo_Placa(y);
                    placa.setColor("Rojo");
                    break;
            }
            return true;
        }else{
            System.out.println("Placa No pertenece a ningun Caso especial ");
            return false;
        }
    }
    
    private boolean ValidarPL_P(Placa placa){
        String x = Ciudad.get(placa.getPL_placa());
        if(x != null){
            placa.setLugar_Matriculacion(x);
            return true;
        }else{
            System.out.println("Placa No pertenece a ninguna provincia ");
            return false;
        }
    }
    
        private void LLENAR_Tipo(){
        Tipo.put("A","Vehiculos Comerciales");
        Tipo.put("U","Vehiculos Comerciales");
        Tipo.put("Z","Vehiculos Comerciales");
        Tipo.put("E","Vehiculos Gubernamentales");
        Tipo.put("X","Vehiculos Uso Oficial");
        Tipo.put("M","Vehiculos Gobiernos Autonomos D");
        
        Tipo.put("CC","Servicio Diplomatico");
        Tipo.put("CD","Servicio Diplomatico");
        Tipo.put("OI","Servicio Diplomatico");
        Tipo.put("AT","Servicio Diplomatico");
        
        Tipo.put("IT","Internacion Temporal");
        
    }
    
    private void LLENAR_Ciudad(){
        Ciudad.put("A","Azuay");
        Ciudad.put("B","Bolivar");
        Ciudad.put("U","Cañar");
        Ciudad.put("C","Carchi");
        Ciudad.put("H","Chimborazo");
        Ciudad.put("X","Cotopaxi");
        Ciudad.put("O","El Oro");
        Ciudad.put("E","Esmeralda");
        Ciudad.put("W","Galapagos");
        Ciudad.put("G","Guayas");
        Ciudad.put("I","Imbabura");
        Ciudad.put("L","Loja");
        Ciudad.put("R","Los Rios");
        Ciudad.put("M","Manabi");
        Ciudad.put("V","Morona Santiago");
        Ciudad.put("N","Napo");
        Ciudad.put("Q","Orellana");
        Ciudad.put("S","Pastaza");
        Ciudad.put("P","Pichincha");
        Ciudad.put("Y","Santa Elena");
        Ciudad.put("J","Santo Domingo T.");
        Ciudad.put("K","Sucumbios");
        Ciudad.put("T","Tungurahua");
        Ciudad.put("Z","Zamora Chinchipe");
        
    }
}
